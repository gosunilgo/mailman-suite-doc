======================
Configure Web Frontend
======================

Mailman 3 has a web frontend which can be used to administer the Core, manage
subscriptions and view the Archives. There are two principal components,
Postorius, the Mailman's web frontend, and Hyperkitty, the Mailman's official
Archiver.

Both Postorius and Hyperkitty are built atop Django, a Python based web
framework.

.. seealso:: :ref:`What is Django? <django-knowledge>`

Django is generally configured using a python module called ``settings.py``
which is usually present at the root of the Django project. It doesn't have to
be at the root of the project, but anywhere *importable* by python.

Assuming that you have already cloned or installed the Django project and know
their location, we can now start configuring Mailman Components. If you haven't
have a look at :ref:`Setting-up-Django-Project` .


.. _setup-admin:

Setting up Admin Account
------------------------

To create a superuser account in Django, run the following interactive command::

  $ cd mailman-suite_project
  $ python manage.py createsuperuser

Before you can login with this user, you have to configure Django to send
emails, so that it can verify the email provided for the superuser account
above.

In Postorius, a superuser is assumed to be the Site Owner and has access to all
the domains, mailing lists and their settings. List Owners and Moderators can be
added based on per-list basis and don't need to have a superuser account in
Django.


.. _setup-email:

Setting up Email (required)
---------------------------

It is important that Django be able to send emails to verify the addresses that
are subscribing to the Mailman. This configuration is separate from what is done
is Core. Please have a look at how to `setup email backend for django`_. A
simple configuration would look something like this for a mail server listening
on localhost::

      # To be added to Django's settings.py

      EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
      EMAIL_HOST = 'localhost'
      EMAIL_PORT = 25
      EMAIL_HOST_USER = <username>
      EMAIL_HOST_PASSWORD = <password>


.. note:: Mailman Suite project in Gitlab disables sending of emails when
          ``DEBUG=True`` is set in ``settings.py``, and instead prints the
          emails to a directory ``emails`` under ``mailman-suite_project``. If
          you don't see any outgoing emails, please check your ``settings.py``
          and set ``DEBUG=False``.

          If you want to sent out emails along with ``DEBUG=True``, you can
          remove the conditional setting at the bottom of ``settings.py`` in
          ``mailman-suite`` project.

          Read more about :setting:`django:DEBUG`.

Here are some settings that determine
how your emails from them look like:

  * :setting:`django:DEFAULT_FROM_EMAIL` : This is the default address that used used as the
    ``FROM`` header in all the emails from your django installation.

  * :setting:`django:SERVER_EMAIL` : This is the address from which the errors emails will be
    sent to you.

.. note:: In order to make django send mails, you need to change
          :setting:`django:EMAIL_BACKEND` from
          ``django.core.mail.backends.console.EmailBackend`` to
          ``django.core.mail.backends.smtp.EmailBackend`` in the `mailman-suite
          <https://gitlab.com/mailman/mailman-suite>`_ project, if you are
          using that.

Note that both of these are general Django related settings and will affect
other parts of your Django installation too.

.. _setup-task-queue:

Running the task queue (required)
---------------------------------
Hyperkitty uses `django_q`_ as a task queue. It supports various different
backends like Redis, Disque, IronMQ, SQS etc. Please check the documentation to
better understand how to configure it. The most basic setup where it uses Django
orm as the queue can be configured using the settings below::

      Q_CLUSTER = {
         'timeout': 300,
         'save_limit': 100,
         'orm': 'default',
      }

You will also have to run `python manage.py qcluster` command along with
Hyperkitty. See Hyperkitty's docs about `asynchronous tasks`_.


.. _setup-search:

Enable full text search (required)
----------------------------------
Hyperkitty uses `django-haystack`_ for its full text search. There are several
full text engines that can be used. See `django-haystack`_ documentation for
more details about the different backend engines that it supports. For the most
basic setup, you can use `whoosh` backend. To install the library try::

  (venv)$ pip install whoosh

Then add the following configuration to the Django's settings.py to enable
whoosh engine for full text search.::


      HAYSTACK_CONNECTIONS = {
            'default': {
            'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
            'PATH': os.path.join(BASE_DIR, "fulltext_index"),
            # You can also use the Xapian engine, it's faster and more accurate,
            # but requires another library.
            # http://django-haystack.readthedocs.io/en/v2.4.1/installing_search_engines.html#xapian
            # Example configuration for Xapian:
            #'ENGINE': 'xapian_backend.XapianEngine'
            },
      }

.. _setup-cron:

Scheduled Tasks (required)
--------------------------
There are some routine tasks that need to be run alongside Django, most of which
are meant to do some specific routine functions in Hyperkitty. You can add the
following to your crontab run them using other cron-like interfaces.::


  # This goes in /etc/cron.d/mailman

  # Replace "apache" by your webserver user ("www-data" on Debian systems) and
  # set the path to the Django project directory

  @hourly  apache  django-admin runjobs hourly  --pythonpath /path/to/project --settings settings
  @daily   apache  django-admin runjobs daily   --pythonpath /path/to/project --settings settings
  @weekly  apache  django-admin runjobs weekly  --pythonpath /path/to/project --settings settings
  @monthly apache  django-admin runjobs monthly --pythonpath /path/to/project --settings settings
  @yearly  apache  django-admin runjobs yearly  --pythonpath /path/to/project --settings settings
  * * * * *  apache  django-admin runjobs minutely  --pythonpath /path/to/project --settings settings
  2,17,32,47 * * * * apache  django-admin runjobs quarter_hourly --pythonpath /path/to/project --settings settings


To Check what jobs do exist and will run on scheduled time, you can run::

  $ python manage.py runjobs -l
  Job List: 11 jobs
   appname           - jobname                - when     - help
  --------------------------------------------------------------------------------
   django_extensions - cache_cleanup          - daily    - Cache (db) cleanup Job
   django_extensions - daily_cleanup          - daily    - Django Daily Cleanup Job
   hyperkitty        - empty_threads          - monthly  - Remove empty threads
   hyperkitty        - new_lists_from_mailman - hourly   - Import new lists from Mailman
   hyperkitty        - orphan_emails          - daily    - Reattach orphan emails
   hyperkitty        - recent_threads_cache   - daily    - Refresh the recent threads cache
   hyperkitty        - sync_mailman           - daily    - Sync user and list properties with Mailman
   hyperkitty        - thread_order_depth     - yearly   - Compute thread order and depth for all threads
   hyperkitty        - thread_starting_email  - hourly   - Find the starting email when it is missing
   hyperkitty        - update_and_clean_index - monthly  - Update the full-text index and clean old entries
   hyperkitty        - update_index           - minutely - Update the full-text index


.. _setup-database:

Setting up Database (required)
------------------------------
Django supports a wide variety of databases and you can have a look at the
documentation for `different options and ways to configure your database to use
with Django <https://docs.djangoproject.com/en/1.11/ref/databases/>`_.


.. _setup-django-login:

Configure Login to Django
-------------------------
Postorius & Hyperkitty both use django-allauth for authentication because it
supports a wire variety of social providers and also allows users to sign up
with their email if they desire.

Note that if you have any problems with the account signup/signin related
emails, you should look the `documentation for django-allauth
<http://django-allauth.readthedocs.io/en/latest/installation.html>`_.

Some of the very basic settings that are required to be set for Postorius &
Hyperkitty to work are mentioned below:

  * ``ACCOUNT_AUTHENTICATION_METHOD`` = "username_email"
  * ``ACCOUNT_EMAIL_REQUIRED`` = True
  * ``ACCOUNT_EMAIL_VERIFICATION`` = "mandatory"
  * ``ACCOUNT_DEFAULT_HTTP_PROTOCOL`` = "http"
  * ``ACCOUNT_UNIQUE_EMAIL``  = True

.. _setup-social-auth:

Configure Social Login
----------------------

Yes, so the way social login works in Mailman is by using a library
called `django-allauth`_. There are a few social providers already
"enabled" in the Django configuration for the container images, to add
more you would have change INSTALLED_APPS in your
settings_local.py. There is no way to "add" any apps as the one
defined in settings_local.py will override the original one (they are
just python variables), so you'd have to copy the entire INSTALLED_APPS3
and then add whatever new ones you want.

To see a list of all the providers, please have a look at the
documentation of `django-allauth`_. Make sure that the one you choose
provides "email" as part of user data, otherwise it won't work with
Mailman. e.g. Twitter doesn't give out emails.

Now to be able to use any provider, you'd have to configure them in your
site. django-allauth documentation2 does provide instructions and
direct urls to configure each one. To summarize the documentation here
are the steps:


.. _django-allauth: http://django-allauth.readthedocs.io/en/latest/index.html


Go to your Django admin interface (located at '/admin', like
http://example.com/admin) and login with whatever superuser you created.  Scroll
down to the section "SOCIAL ACCOUNTS" and enter the one saying "Social
applications".The relative url for that is '/admin/socialaccount/socialapp/'
Click on "ADD SOCIAL APPLICATION" button on the top left of the page.  Fill out
the details from your social provider and choose the "Sites" you want to use for
that particular social provider. You can have separate credentials for separate
"Sites".


That should be all. Make sure when you request client_id and
client_secret from the social providers, you provide the correct
callback_url. The documentation for django-allauth has the correct
url for each provider, but it looks basically like

http://mysite.com/accounts/<provider>/login/callback

and you replace <provider> with amazon, facebook, google or whatever
provider.


.. _mailman-web-settings:

Configure Postorius & Hyperkitty
---------------------------------

Here are the parameters that will affect Postorius and Hyperkitty will
function. These parameters are configured in your Django's ``settings.py``.

.. setting:: MAILMAN_REST_API_URL

``MAILMAN_REST_API_URL``
    Complete URL to the Core's REST API Server. Usually, Mailman Core listens
    on port 8001 for REST API. e.g. ``http://localhost:8001/``

.. setting:: MAILMAN_REST_API_USER

``MAILMAN_REST_API_USER``
    Username for the Core's REST API, default value in core is 'restadmin' if
    not set.

.. setting:: MAILMAN_REST_API_PASS

``MAILMAN_REST_API_PASS``
    Password for Mailman REST API User, default value in core is 'restpass' if
    not set.

.. setting:: MAILMAN_ARCHIVER_KEY

``MAILMAN_ARCHIVER_KEY``
    The Key used to authenticate the emails for archiving in Hyperkitty. Its
    value should be exactly same as set in Core.

    Also note that the value in ``settings.py`` will be within single quotes,
    but in ``mailman.cfg`` it will be without any quotes.

.. setting:: FILTER_VHOST

``FILTER_VHOST``
    Filter the list of available lists in Postorius and Hyperkitty depending on
    the domain they are being currently served from.  Mailman 3 supports
    multiple domains in a single installation.

.. setting:: LOGIN_URL

``LOGIN_URL``
    'account_login'

.. setting:: LOGIN_REDIRECT_URL

``LOGIN_REDIRECT_URL``
    'list_index'

.. setting:: LOGOUT_URL

``LOGOUT_URL``
    'account_logout'

.. setting:: STATICFILE_FINDERS

``STATICFILE_FINDERS``
    Add ``compressor.finders.CompressorFinder`` to your ``STATICFILES_FINDERS``.

	See also :setting:`django:STATICFILE_FINDERS`.

.. setting:: COMPRESS_PRECOMPILERS

``COMPRESS_PRECOMPILERS``
    This setting is for django-compressor which is used here to compile and
    compress static files::

      COMPRESS_PRECOMPILERS = (
        ('text/x-scss', 'sassc -t compressed {infile} {outfile}'),
        ('text/x-sass', 'sassc -t compressed {infile} {outfile}'),
      )

.. setting:: POSTORIUS_TEMPLATE_BASE_URL

``POSTORIUS_TEMPLATE_BASE_URL``
    should be set to URL that Postorius is expected to be listening at. You
    should set it to whatever URL your WSGI server is listening at.


.. _setup email backend for django: https://docs.djangoproject.com/en/1.11/topics/email/
.. _django-haystack : https://django-haystack.readthedocs.io/en/master/backend_support.html
.. _django_q : https://django-q.readthedocs.io/en/latest/
.. _asynchronous tasks: http://hyperkitty.readthedocs.io/en/latest/install.html#asynchronous-tasks
